//Imports
const express = require('express');
const app = express();
const port = 3001;

//Set Views
app.set('views', './views');
app.set('view engine', 'ejs');

app.get('', (req, res) => {
    res.render('index');
});

//Listen on port
app.listen(port, () => console.log(`Listening on port ${port}`));

